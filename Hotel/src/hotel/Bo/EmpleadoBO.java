/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Bo;

import hotel.Dao.EmpleadoDAO;
import hotel.Entities.Empleado;
import hotel.Entities.MiError;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class EmpleadoBO {
    public boolean insertar(Empleado e){
        if(e.getCedula() == null){
            throw new MiError("Favor ingresar el número de indentificación");
        }else if(e.getEstado() == false){
            throw new MiError("Favor elegir el estado del empleado");
        }
        EmpleadoDAO edao = new EmpleadoDAO();
        return edao.insertar(e);
    }
}
