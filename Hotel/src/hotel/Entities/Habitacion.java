/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Entities;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class Habitacion {
    private int id;
    private boolean estado;
    private String numPiso;
    private int numHabitacion;
    private TipoHabitacion idtipo;

    public Habitacion() {
    }

    public Habitacion(int id, boolean estado, String numPiso, int numHabitacion, TipoHabitacion idtipo) {
        this.id = id;
        this.estado = estado;
        this.numPiso = numPiso;
        this.numHabitacion = numHabitacion;
        this.idtipo = idtipo;
    }

    public int getId() {
        return id;
    }

    public boolean isEstado() {
        return estado;
    }

    public String getNumPiso() {
        return numPiso;
    }

    public int getNumHabitacion() {
        return numHabitacion;
    }

    public TipoHabitacion getIdtipo() {
        return idtipo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public void setNumPiso(String numPiso) {
        this.numPiso = numPiso;
    }

    public void setNumHabitacion(int numHabitacion) {
        this.numHabitacion = numHabitacion;
    }

    public void setIdtipo(TipoHabitacion idtipo) {
        this.idtipo = idtipo;
    }

    @Override
    public String toString() {
        return "Habitacion{" + "id=" + id + ", estado=" + estado + ", numPiso=" + numPiso + ", numHabitacion=" + numHabitacion + ", idtipo=" + idtipo + '}';
    }
    
}
