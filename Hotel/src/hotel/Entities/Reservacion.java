/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Entities;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class Reservacion {
    private int id;
    private Agencia idAgencia;
    private Habitacion idHabitacion;
    private int fechaI;
    private int fechaS;
    private int estado;
    private int comiciones;

    public Reservacion() {
    }
    public Reservacion(int id, Agencia idAgencia, Habitacion idHabitacion, int fechaI, int fechaS, int estado, int comiciones) {
        this.id = id;
        this.idAgencia = idAgencia;
        this.idHabitacion = idHabitacion;
        this.fechaI = fechaI;
        this.fechaS = fechaS;
        this.estado = estado;
        this.comiciones = comiciones;
    }

    public int getId() {
        return id;
    }
    public Agencia getIdAgencia() {
        return idAgencia; 
    }
    public Habitacion getIdHabitacion() {
        return idHabitacion;
    }
    public int getFechaI() {
        return fechaI;
    }
    public int getFechaS() {
        return fechaS;
    }
    public int getEstado() {
        return estado;
    }
    public int getComiciones() {
        return comiciones;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdAgencia(Agencia idAgencia) {
        this.idAgencia = idAgencia;
    }
    public void setIdHabitacion(Habitacion idHabitacion) {
        this.idHabitacion = idHabitacion;
    }
    public void setFechaI(int fechaI) {
        this.fechaI = fechaI;
    }
    public void setFechaS(int fechaS) {
        this.fechaS = fechaS;
    }
    public void setEstado(int estado) {
        this.estado = estado;
    }
    public void setComiciones(int comiciones) {
        this.comiciones = comiciones;
    }

    @Override
    public String toString() {
        return "Reservacion{" + "id=" + id + ", idAgencia=" + idAgencia + ", idHabitacion=" + idHabitacion + ", fechaI=" + fechaI + ", fechaS=" + fechaS + ", estado=" + estado + ", comiciones=" + comiciones + '}';
    }   
}