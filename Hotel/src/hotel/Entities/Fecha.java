/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Entities;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class Fecha {
    private int id;
    private int dia;
    private String mes;
    private int anno;

    public Fecha() {
    }
    public Fecha(int id, int dia, String mes, int anno) {
        this.id = id;
        this.dia = dia;
        this.mes = mes;
        this.anno = anno;
    }

    public int getId() {
        return id;
    }
    public int getDia() {
        return dia;
    }
    public String getMes() {
        return mes;
    }
    public int getAnno() {
        return anno;
    }

    public void setId(int id) {
        this.id = id;
    }
    public void setDia(int dia) {
        this.dia = dia;
    }
    public void setMes(String mes) {
        this.mes = mes;
    }
    public void setAnno(int anno) {
        this.anno = anno;
    }

    @Override
    public String toString() {
        return "Fecha{" + "id=" + id + ", dia=" + dia + ", mes=" + mes + ", anno=" + anno + '}';
    }
}
