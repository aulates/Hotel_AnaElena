/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Entities;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class Factura {
    private int id;
    private Reservacion idReservacion;
    private float pagoTotal;
    private int descuentoTotal;
    private String medioPago;
    private Cliente idCliente;
    private Fecha idFecha;

    public Factura() {
    }

    public Factura(int id, Reservacion idReservacion, float pagoTotal, int descuentoTotal, String medioPago, Cliente idCliente, Fecha idFecha) {
        this.id = id;
        this.idReservacion = idReservacion;
        this.pagoTotal = pagoTotal;
        this.descuentoTotal = descuentoTotal;
        this.medioPago = medioPago;
        this.idCliente = idCliente;
        this.idFecha = idFecha;
    }

    public int getId() {
        return id;
    }
    public Reservacion getIdReservacion() {
        return idReservacion;
    }
    public float getPagoTotal() {
        return pagoTotal;
    }
    public int getDescuentoTotal() {
        return descuentoTotal;
    }
    public String getMedioPago() {
        return medioPago;
    }
    public Cliente getIdCliente() {
        return idCliente;
    }
    public Fecha getIdFecha() {
        return idFecha;
    }

    public void setId(int id) {
        this.id = id;
    }
    public void setIdReservacion(Reservacion idReservacion) {
        this.idReservacion = idReservacion;
    }
    public void setPagoTotal(float pagoTotal) {
        this.pagoTotal = pagoTotal;
    }
    public void setDescuentoTotal(int descuentoTotal) {
        this.descuentoTotal = descuentoTotal;
    }
    public void setMedioPago(String medioPago) {
        this.medioPago = medioPago;
    }
    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }
    public void setIdFecha(Fecha idFecha) {
        this.idFecha = idFecha;
    }

    @Override
    public String toString() {
        return "Factura{" + "id=" + id + ", idReservacion=" + idReservacion + ", pagoTotal=" + pagoTotal + ", descuentoTotal=" + descuentoTotal + ", medioPago=" + medioPago + ", idCliente=" + idCliente + ", idFecha=" + idFecha + '}';
    }
    
}
