/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Entities;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class Puesto {
    private int id;
    private int codPuesto;
    private int estado;
    private int salariobase;
    private String tipoPuesto;

    public Puesto() {
    }

    public Puesto(int id, int codPuesto, int estado, int salariobase, String tipoPuesto) {
        this.id = id;
        this.codPuesto = codPuesto;
        this.estado = estado;
        this.salariobase = salariobase;
        this.tipoPuesto = tipoPuesto;
    }

    public int getId() {
        return id;
    }
    public int getCodPuesto() {
        return codPuesto;
    }
    public int getEstado() {
        return estado;
    }
    public int getSalariobase() {
        return salariobase;
    }
    public String getTipoPuesto() {
        return tipoPuesto;
    }

    public void setId(int id) {
        this.id = id;
    }
    public void setCodPuesto(int codPuesto) {
        this.codPuesto = codPuesto;
    }
    public void setEstado(int estado) {
        this.estado = estado;
    }
    public void setSalariobase(int salariobase) {
        this.salariobase = salariobase;
    }
    public void setTipoPuesto(String tipoPuesto) {
        this.tipoPuesto = tipoPuesto;
    }

    @Override
    public String toString() {
        return id + tipoPuesto;
    }   
}