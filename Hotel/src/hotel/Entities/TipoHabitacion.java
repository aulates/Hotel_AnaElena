/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Entities;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class TipoHabitacion {
    private int id;
    private int cantCamas;
    private String tipocamas;
    private String tipoCuarto;
    private int precio;

    public TipoHabitacion() {
    }

    public TipoHabitacion(int id, int cantCamas, String tipocamas, String tipoCuarto, int precio) {
        this.id = id;
        this.cantCamas = cantCamas;
        this.tipocamas = tipocamas;
        this.tipoCuarto = tipoCuarto;
        this.precio = precio;
    }

    public int getId() {
        return id;
    }

    public int getCantCamas() {
        return cantCamas;
    }

    public String getTipocamas() {
        return tipocamas;
    }

    public String getTipoCuarto() {
        return tipoCuarto;
    }

    public int getPrecio() {
        return precio;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCantCamas(int cantCamas) {
        this.cantCamas = cantCamas;
    }

    public void setTipocamas(String tipocamas) {
        this.tipocamas = tipocamas;
    }

    public void setTipoCuarto(String tipoCuarto) {
        this.tipoCuarto = tipoCuarto;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "TipoHabitacion{" + "id=" + id + ", cantCamas=" + cantCamas + ", tipocamas=" + tipocamas + ", tipoCuarto=" + tipoCuarto + ", precio=" + precio + '}';
    }
    
}
