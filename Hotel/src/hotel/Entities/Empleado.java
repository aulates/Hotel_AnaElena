/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Entities;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class Empleado{
    private String cedula;
    private boolean estado;
    private Puesto puesto;

    public Empleado() {
    }

    public Empleado(String cedula, boolean estado, Puesto puesto) {
        this.cedula = cedula;
        this.estado = estado;
        this.puesto = puesto;
    }

    public String getCedula() {
        return cedula;
    }
    public boolean getEstado() {
        return estado;
    }
    public Puesto getPuesto() {
        return puesto;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    public void setPuesto(Puesto puesto) {
        this.puesto = puesto;
    }
   

    @Override
    public String toString() {
        return "Empleado{" + "cedula=" + cedula + ", estado=" + estado +  ", puesto=" + puesto + '}';
    }
    

    
}
