/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Entities;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class Cliente {
    private int id;
    private String cedula;
    private String nombre;
    private Telefono idtelefono;

    public Cliente() {
    }

    public Cliente(int id, String cedula, String nombre, Telefono idtelefono) {
        this.id = id;
        this.cedula = cedula;
        this.nombre = nombre;
        this.idtelefono = idtelefono;
    }
    
    public int getId() {
        return id;
    }
    public String getCedula() {
        return cedula;
    }
    public String getNombre() {
        return nombre;
    }
    public Telefono getIdtelefono() {
        return idtelefono;
    }

    public void setId(int id) {
        this.id = id;
    }
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setIdtelefono(Telefono idtelefono) {
        this.idtelefono = idtelefono;
    }

    @Override
    public String toString() {
        return "Cliente{" + "id=" + id + ", cedula=" + cedula + ", nombre=" + nombre + ", idtelefono=" + idtelefono + '}';
    }
    
   
   
}
