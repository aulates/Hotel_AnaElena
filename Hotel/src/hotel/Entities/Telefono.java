/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Entities;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class Telefono {
    private int id;
    private int identificador;
    private String telefono;

    public Telefono() {
    }
    public Telefono(int id, int identificador, String telefono) {
        this.id = id;
        this.identificador = identificador;
        this.telefono = telefono;
    }

    public int getId() {
        return id;
    }
    public int getIdentificador() {
        return identificador;
    }
    public String getTelefono() {
        return telefono;
    }

    public void setId(int id) {
        this.id = id;
    }
    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "Telefono{" + "id=" + id + ", identificador=" + identificador + ", telefono=" + telefono + '}';
    }
    
}
