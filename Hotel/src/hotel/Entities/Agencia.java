/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Entities;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class Agencia {
    private int id;
    private String codAgencia;
    private String nombre;

    public Agencia() {
    }
    public Agencia(int id, String codAgencia, String nombre) {
        this.id = id;
        this.codAgencia = codAgencia;
        this.nombre = nombre;
    }
    
    public int getId() {
        return id;
    }
    public String getCodAgencia() {
        return codAgencia;
    }
    public String getNombre() {
        return nombre;
    }
    
    
    public void setId(int id) {
        this.id = id;
    }
    public void setCodAgencia(String codAgencia) {
        this.codAgencia = codAgencia;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Agencia{" + "id=" + id + ", codAgencia=" + codAgencia + ", nombre=" + nombre + '}';
    }
    
    
}
