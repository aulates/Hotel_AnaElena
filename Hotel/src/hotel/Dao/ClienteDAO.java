/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Dao;

import hotel.Entities.Cliente;
import hotel.Entities.MiError;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class ClienteDAO {
     public boolean insertar(Cliente c) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into cliente(identificacion, nombre, idtelefono) "
                    + "values (?,?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(2, c.getCedula());
            stmt.setString(3, c.getNombre());
            stmt.setInt(3, c.getIdtelefono().getIdentificador());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) { 
            ex.printStackTrace();// TODO: Eliminar esta  línea
            throw new MiError("Problemas al registrar el empleado, favor intente nuevamente");
        }
    }
    public LinkedList<Cliente> seleccionarTodo() {
        LinkedList<Cliente> cliente = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from cliente";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                cliente.add(cargarCliente(rs));
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al seleccionar al cliente, favor intente nuevamente");
        }

        return cliente;
    }

    public Cliente cargarCliente(ResultSet rs) throws SQLException {
        Cliente c = new Cliente();
        c.setId(rs.getInt("id"));
        c.setCedula(rs.getString("cedula"));
        c.setNombre(rs.getString("nombre"));
        //Cargar la foranea
        TelefonoDAO  tdao = new TelefonoDAO();
        c.setIdtelefono((tdao.cargarIDTelefono(rs.getInt("idtelefono"))));
        return c;
    }
    public Cliente cargarid(String identificador) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from telefono where identificador = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, identificador);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return cargarCliente(rs);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar los clientes, favor intente nuevamente");
        }
        return null;
    }
}
