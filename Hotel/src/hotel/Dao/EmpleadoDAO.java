/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Dao;

import hotel.Entities.Empleado;
import hotel.Entities.MiError;
import hotel.Entities.Puesto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class EmpleadoDAO {
    public boolean insertar(Empleado e) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into empleado( cedula, estado, codpuesto) "
                    + "values (?,?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, e.getCedula());
            stmt.setBoolean(2, e.getEstado());
            stmt.setInt(3, e.getPuesto().getCodPuesto());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            ex.printStackTrace();// TODO: Eliminar esta  línea
            throw new MiError("Problemas al registrar el empleado, favor intente nuevamente");
        }
    }
    
    public boolean editarEstado(String cedula, boolean estado) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "update empleado set estado = ? where cedula = ? ";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setBoolean(1, estado);
            stmt.setString(2, cedula);
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            ex.printStackTrace();// TODO: Eliminar esta  línea
            throw new MiError("Problemas al editar el empleado, favor intente nuevamente");
        }
    }
    public LinkedList<Empleado> seleccionarTodo() {
        LinkedList<Empleado> empleado = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from empleado";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                empleado.add(cargarEmpleado(rs));
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar los empleados, favor intente nuevamente");
        }

        return empleado;
    }

    private Empleado cargarEmpleado(ResultSet rs) throws SQLException {
        Empleado e = new Empleado();
        e.setCedula(rs.getString("cedula"));
        e.setEstado(rs.getBoolean("estado"));
        
        //Cargar la foranea
        PuestosDAO pdao = new PuestosDAO();
        e.setPuesto(pdao.cargarTipoPuesto(rs.getString("codpuesto")));
        
        return e;
    }
    public Empleado cargarIdEmpleado(int id) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from empleado where cedula = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return EmpleadoDAO.this.cargarEmpleado(rs);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar los empleados,"
                    + " favor intente nuevamente");
        }
        return null;
    }
}
