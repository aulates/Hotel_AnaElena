/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Dao;

import hotel.Entities.Agencia;
import hotel.Entities.MiError;
import hotel.Entities.Telefono;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class TelefonoDAO {
    
    public LinkedList<Telefono> seleccionarTodo() {
        LinkedList<Telefono> telefono = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from telefono";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                telefono.add(cargarTelefono(rs));
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar los telefonos, favor intente nuevamente");
        }

        return telefono;
    }
    
    public boolean insertar(Telefono t) {
            try (Connection con = Conexion.getConexion()) {
                String sql = "insert into telefono(identificador, telefono)"
                        + "values (?,?)";
                PreparedStatement stmt = con.prepareStatement(sql);
                stmt.setString(2, t.getTelefono());
                stmt.setInt(1, t.getIdentificador());
                return stmt.executeUpdate() > 0;
            } catch (Exception ex) {
                ex.printStackTrace();// TODO: Eliminar esta  línea
                throw new MiError("Problemas al registrar la agencia"
                        + ", favor intente nuevamente");
            }
    }
    
    public Telefono cargarIDTelefono(int identificador) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from telefono where identificador = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, identificador);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return cargarTelefono(rs);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar los puestos, favor intente nuevamente");
        }
        return null;
    }
    private Telefono cargarTelefono(ResultSet rs) throws SQLException {
        Telefono t = new Telefono();
        t.setId(rs.getInt("idregistro"));
        t.setIdentificador(rs.getInt("identificardor"));
        t.setTelefono(rs.getString("telefono"));
        return t;
    }
}
