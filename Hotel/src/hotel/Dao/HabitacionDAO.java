/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Dao;

import hotel.Entities.Agencia;
import hotel.Entities.Habitacion;
import hotel.Entities.MiError;
import hotel.Entities.TipoHabitacion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class HabitacionDAO {
    public boolean insertar(Habitacion h) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into habitacion(estado, numeroPiso,"
                    + " numHabitacion, idTipoHabitacion)"
                    + "values (?,?,?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setBoolean(1,h.isEstado());
            stmt.setString(2, h.getNumPiso());
            stmt.setInt(3, h.getNumHabitacion());
            stmt.setInt(4, h.getIdtipo().getId());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            ex.printStackTrace();// TODO: Eliminar esta  línea
            throw new MiError("Problemas al registrar las habitaciones"
                    + ", favor intente nuevamente");
        }
    }
    private Habitacion cargarHabitaciones(ResultSet rs) throws SQLException {
        Habitacion p = new Habitacion();
        p.setId(rs.getInt("idregistro"));
        p.setEstado(rs.getBoolean("estado"));
        p.setNumHabitacion(rs.getInt("numhabitacion"));
        p.setNumPiso(rs.getString("numeropiso"));
        
        TipoHabitacionDAO th = new TipoHabitacionDAO();
        p.setIdtipo(th.cargarID(rs.getInt("idtipohabitacion")));
        
      
        return p;
    }
    
    public Habitacion cargarTipoHabitaciones(int idTipoHabitacion) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from habitacion where idTipoHabitacion = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, idTipoHabitacion);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return cargarHabitaciones(rs);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar las habitaciones, favor intente nuevamente");
        }
        return null;
    }
    /*
    } public LinkedList<Puesto> seleccionarTodo() {
        LinkedList<Puesto> puesto = new LinkedList<>();
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from puestos";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                puesto.add(cargarPuestos(rs));
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar los puestos, favor intente nuevamente");
        }

        return puesto;
    }
    */
    public LinkedList<Habitacion> seleccionarTodo() {
        LinkedList<Habitacion> habitaciones = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from habitacion";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                habitaciones.add(cargarHabitaciones(rs));
            }
            
            return habitaciones;
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            ex.printStackTrace();
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar la habitaciones, favor intente nuevamente");
        }

        
    }
}
