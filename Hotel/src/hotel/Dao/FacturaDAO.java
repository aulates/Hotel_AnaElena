/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Dao;

import hotel.Entities.Agencia;
import hotel.Entities.Factura;
import hotel.Entities.MiError;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class FacturaDAO {
    
     public boolean insertar(Factura f) {
            try (Connection con = Conexion.getConexion()) {
                String sql = "insert into factura(idReservacion, pagoTotal, descuentoTotal,"
                        + "medioPago, idCliente, idFecha)"
                        + "values (?,?,?,?,?,?)";
                PreparedStatement stmt = con.prepareStatement(sql);
                stmt.setInt(1, f.getIdReservacion().getId());
                stmt.setFloat(2, f.getPagoTotal());
                stmt.setInt(3, f.getDescuentoTotal());
                stmt.setString(4, f.getMedioPago());
                stmt.setInt(5, f.getIdCliente().getId());
                stmt.setInt(6, f.getIdFecha().getId());
                return stmt.executeUpdate() > 0;
            } catch (Exception ex) {
                ex.printStackTrace();// TODO: Eliminar esta  línea
                throw new MiError("Problemas al guardar las facturas"
                        + ", favor intente nuevamente");
            }
    }
    
    
   private Factura cargarFacturas(ResultSet rs) throws SQLException {
        Factura f = new Factura();
        f.setId(rs.getInt("idregistro"));
        f.setDescuentoTotal(rs.getInt("descuentoTotal"));
        f.setMedioPago(rs.getString("medioPago"));
        f.setPagoTotal(rs.getFloat("pagoTotal"));
        
        ClienteDAO cdao = new ClienteDAO();
        f.setIdCliente(cdao.cargarid(rs.getString("idCliente")));
        // e.setIdtelefono((tdao.cargarTelefono(rs.getInt("idtelefono"))));
        return f;
    }
    
    public Factura cargar(int id) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from Factura where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return FacturaDAO.this.cargarFacturas(rs);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar la agencia,"
                    + " favor intente nuevamente");
        }
        return null;
    }
    public LinkedList<Factura> seleccionarTodo() {
        LinkedList<Factura> factura = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from factura";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                factura.add(cargarFacturas(rs)); 
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar al cliente, favor intente nuevamente");
        }

        return factura;
    }
}
