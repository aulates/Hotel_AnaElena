/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Dao;

import hotel.Entities.MiError;
import hotel.Entities.Telefono;
import hotel.Entities.TipoHabitacion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class TipoHabitacionDAO {
    
      public LinkedList<TipoHabitacion> seleccionarTodo() {
        LinkedList<TipoHabitacion> tipoHabitacion = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from tipoHabitacion";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                tipoHabitacion.add(cargarTipoHabitacion(rs));
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar los tipos de habitaciones,"
                    + " favor intente nuevamente");
        }

        return tipoHabitacion;
    }
    
    public boolean insertar(TipoHabitacion th) {
            try (Connection con = Conexion.getConexion()) {
                String sql = "insert into tipoHabitacion(cantidadCamas, "
                        + "tipoCamas, tipoHabitacion, precio)"
                        + "values (?,?,?, ?)";
                PreparedStatement stmt = con.prepareStatement(sql);
                stmt.setInt(1, th.getCantCamas());
                stmt.setString(2, th.getTipocamas());
                stmt.setString(3, th.getTipoCuarto());
                stmt.setFloat(4, th.getPrecio());
                return stmt.executeUpdate() > 0;
            } catch (Exception ex) {
                ex.printStackTrace();// TODO: Eliminar esta  línea
                throw new MiError("Problemas al guardar los tipos de habitaciones"
                        + ", favor intente nuevamente");
            }
    }
    
    //Error hay que cambiar los nombres de las columnas
    
    private TipoHabitacion cargarTipoHabitacion(ResultSet rs) throws SQLException {
        TipoHabitacion th = new TipoHabitacion();
        th.setId(rs.getInt("idRegistro"));
        th.setCantCamas(rs.getInt("cantidadCamas"));
        th.setPrecio(rs.getInt("precio"));
        th.setTipoCuarto(rs.getString("tipoCamas"));
        th.setTipoCuarto(rs.getString("tipoHabitacion"));
        return th;
    }

    public TipoHabitacion cargarID(int id) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from tipoHabitacio where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return cargarTipoHabitacion(rs);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar los tipos de habitacion, favor intente nuevamente");
        }
        return null;
    }
    
}
