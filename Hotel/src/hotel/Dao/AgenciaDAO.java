/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Dao;

import hotel.Entities.Agencia;
import hotel.Entities.MiError;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class AgenciaDAO {
      public boolean insertar(Agencia a) {
            try (Connection con = Conexion.getConexion()) {
                String sql = "insert into agencia(codAgencia, descripcion)"
                        + "values (?,?)";
                PreparedStatement stmt = con.prepareStatement(sql);
                stmt.setString(1, a.getCodAgencia());
                stmt.setString(2, a.getNombre());
                return stmt.executeUpdate() > 0;
            } catch (Exception ex) {
                ex.printStackTrace();// TODO: Eliminar esta  línea
                throw new MiError("Problemas al registrar la agencia"
                        + ", favor intente nuevamente");
            }
    }
    
    
   private Agencia cargarAgencia(ResultSet rs) throws SQLException {
        Agencia a = new Agencia();
        a.setId(rs.getInt("id_registro"));
        a.setCodAgencia(rs.getString("codAgencia"));
        a.setNombre(rs.getString("descripcion"));
        return a;
    }
    
    public Agencia cargarAgencia(int id) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from agencia where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return AgenciaDAO.this.cargarAgencia(rs);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar la agencia,"
                    + " favor intente nuevamente");
        }
        return null;
    }
    public LinkedList<Agencia> seleccionarTodo() {
        LinkedList<Agencia> agencia = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from agencia";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                agencia.add(cargarAgencia(rs));
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar la Agencia, favor intente nuevamente");
        }

        return agencia;
    }
}
