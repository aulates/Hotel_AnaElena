/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.Dao;

import hotel.Entities.MiError;
import hotel.Entities.Puesto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Ana Elena Ulate Salas
 */
public class PuestosDAO {
    
    public boolean insertar(Puesto p) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into puestos(codpuesto, estado, salario, "
                    + "tipopuesto) "
                    + "values (?,?,?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(2, p.getCodPuesto());
            stmt.setInt(3, p.getEstado());
            stmt.setInt(4, p.getSalariobase());
            stmt.setString(5, p.getTipoPuesto());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            ex.printStackTrace();// TODO: Eliminar esta  línea
            throw new MiError("Problemas al registrar los puestos"
                    + ", favor intente nuevamente");
        }
    }
    
    
   private Puesto cargarPuestos(ResultSet rs) throws SQLException {
        Puesto p = new Puesto();
        p.setId(rs.getInt("idregistro"));
        p.setTipoPuesto(rs.getString("tipopuesto"));
        p.setCodPuesto(rs.getInt("codpuesto"));
        p.setSalariobase(rs.getInt("salario"));
        return p;
    }
    
    public Puesto cargarTipoPuesto(String tipo_puesto) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from puestos where tipopuesto = ? and estado = true";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, tipo_puesto);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return cargarPuestos(rs);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar los puestos, favor intente nuevamente");
        }
        return null;
    } public LinkedList<Puesto> seleccionarTodo() {
        LinkedList<Puesto> puesto = new LinkedList<>();
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from puestos";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                puesto.add(cargarPuestos(rs));
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar los puestos, favor intente nuevamente");
        }

        return puesto;
    }
    
}
